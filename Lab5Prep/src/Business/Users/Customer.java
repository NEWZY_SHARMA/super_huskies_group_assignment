/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import java.util.Date;

/**
 *
 *
 */
public class Customer extends User{
      
    private Date dateCreated;
    private String contactNumber;
   
    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    
   public Customer(String password, String userName, String role){
       super(password, userName, role);
   }
    
   public boolean verify(String password){
       return true;
    }
  
}
